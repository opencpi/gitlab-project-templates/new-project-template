# New Project Template

The project template that all new projects will be created from.

Defines a default set of issue templates, branches and other project settings
to reduce the overhead of configuring new projects.

## Things to do
- [ ] Update [CONTRIBUTING.md](CONTRIBUTING.md)
- [ ] Read this: https://www.opencpi.org/services
- [ ] Read this: https://www.opencpi.org/documentation
- [ ] Change this [README file](README.md)
